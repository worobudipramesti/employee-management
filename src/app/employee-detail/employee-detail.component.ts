import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';

interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: number;
  status: string;
  group: string;
  description: Date;
}

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})


export class EmployeeDetailComponent implements OnInit {
  employeeId: number = 0;
  employee: Employee = {
    id: 0,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: new Date(),
    basicSalary: 0,
    status: '',
    group: '',
    description: new Date
  };
  searchTerm: string = '';
  itemsPerPage: number = 10;
  currentPage: number = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.employee = this.employeeService.getEmployeeById(Number(id));
    }
    this.route.queryParams.subscribe(params => {
      this.searchTerm = params['searchTerm'] || '';
      this.itemsPerPage = +params['itemsPerPage'] || 10;
      this.currentPage = +params['page'] || 1;
    });
  }

  loadEmployeeDetail(): void {
    this.employee = this.employeeService.getEmployeeById(this.employeeId);
  }

  backToList(): void {
    this.router.navigate(['/employee-list'], {
      queryParams: {
        searchTerm: this.searchTerm,
        itemsPerPage: this.itemsPerPage,
        page: this.currentPage
      }
    });
  }
}
