import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';

interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  allEmployees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  searchTerm: string = '';
  currentPage: number = 1;
  itemsPerPage: number = 10;
  displayedEmployees: Employee[] = [];
  notificationMessage: string | null = null;
  notificationType: string = '';

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.searchTerm = params['searchTerm'] || '';
      this.itemsPerPage = +params['itemsPerPage'] || 10;
      this.currentPage = +params['page'] || 1;
      this.loadEmployees();
    });
  }

  loadEmployees(): void {
    this.filteredEmployees = this.employeeService.getEmployees().filter(employee => {
      return employee.firstName.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
        employee.lastName.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
        employee.email.toLowerCase().includes(this.searchTerm.toLowerCase());
    });
    this.updateDisplayedEmployees();
  }

  updateDisplayedEmployees() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = this.currentPage * this.itemsPerPage;
    this.displayedEmployees = this.filteredEmployees.slice(startIndex, endIndex);
  }

  sortEmployees(attribute: keyof Employee) {
    this.allEmployees.sort((a, b) => {
      const aValue = a[attribute];
      const bValue = b[attribute];

      if (typeof aValue === 'string' && typeof bValue === 'string') {
        return aValue.localeCompare(bValue);
      } else if (typeof aValue === 'number' && typeof bValue === 'number') {
        return aValue - bValue;
      }

      return 0;
    });
    this.searchEmployees();
  }

  onKeyDownSort(event: KeyboardEvent, attribute: keyof Employee) {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault();
      this.sortEmployees(attribute);
    }
  }

  searchEmployees() {
    this.currentPage = 1;
    this.updateQueryParams();
  }

  setItemsPerPage(event: any): void {
    this.itemsPerPage = +event.target.value;
    this.currentPage = 1;
    this.updateQueryParams();
  }

  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.updateQueryParams();
    }
  }

  nextPage(): void {
    if (this.currentPage * this.itemsPerPage < this.filteredEmployees.length) {
      this.currentPage++;
      this.updateQueryParams();
    }
  }

  updateQueryParams(): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        searchTerm: this.searchTerm,
        itemsPerPage: this.itemsPerPage,
        page: this.currentPage
      },
      queryParamsHandling: 'merge'
    });
    this.updateDisplayedEmployees();
  }

  navigateToAddEmployee() {
    this.router.navigate(['/add-employee']);
  }

  editEmployee(employee: Employee) {
    this.notificationMessage = `Editing ${employee.firstName} ${employee.lastName}`;
    this.notificationType = 'edit';
    setTimeout(() => this.clearNotification(), 3000);
  }

  deleteEmployee(employee: Employee) {
    this.employeeService.deleteEmployee(employee.id);
    this.allEmployees = this.allEmployees.filter(e => e.id !== employee.id);
    this.searchEmployees();
    this.notificationMessage = `Data ${employee.username} berhasil dihapus`;
    this.notificationType = 'delete';
    setTimeout(() => this.clearNotification(), 3000);
  }

  viewEmployeeDetail(id: number): void {
    this.router.navigate(['/employee-detail', id], {
      queryParams: {
        searchTerm: this.searchTerm,
        itemsPerPage: this.itemsPerPage,
        page: this.currentPage
      }
    });
  }

  clearNotification() {
    this.notificationMessage = null;
    this.notificationType = '';
  }
}
