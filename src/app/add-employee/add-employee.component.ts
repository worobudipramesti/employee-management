import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';

interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: number;
  status: string;
  group: string;
  description: Date;
}

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = {
    id: 0,
    username: '', 
    firstName: '', 
    lastName: '', 
    email: '', 
    birthDate: new Date(), 
    basicSalary: 0,
    status: '',
    group: '',
    description: new Date
  };

  groups: string[] = ['Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5', 'Group 6', 'Group 7', 'Group 8', 'Group 9', 'Group 10'];

  constructor(private router: Router, private employeeService:EmployeeService) { }

  ngOnInit() { }

  onSave(form: NgForm) {
    if (form.valid && this.validateEmail(this.employee.email) && this.employee.basicSalary > 0) {
      this.employeeService.addEmployee(this.employee);
      alert('Employee data saved!');
      this.router.navigate(['/employee-list']);
    } else {
      alert('Please fill in all fields correctly.');
    }
  }

  onCancel() {
    this.router.navigate(['/employee-list']);
  }

  validateEmail(email: string) {
    const re = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    return re.test(String(email).toLowerCase());
  }
}
