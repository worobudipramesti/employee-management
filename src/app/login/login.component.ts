import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  errorMessage: string = '';
  loginAttempted: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  onLogin() {
    this.loginAttempted = true;
    if (this.username && this.password) {
      if (this.username === 'admin' && this.password === 'password') {
        this.authService.login();
        this.router.navigate(['/employee-list']);
      } else {
        this.errorMessage = 'Invalid username or password';
      }
    }
  }
}
