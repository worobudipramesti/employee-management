import { Injectable } from '@angular/core';
import { faker } from '@faker-js/faker';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employees: any[] = [];

  constructor() {
    for (let i = 1; i <= 100; i++) {
      const birthDate = this.randomDate(new Date(1980, 0, 1), new Date(2000, 11, 31));
      const descriptionDate = this.randomDate(new Date(2020, 0, 1), new Date());
      
      this.employees.push({
        id: i,
        username: faker.internet.userName(),
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        email: faker.internet.email(),
        birthDate: this.formatDate(birthDate),
        basicSalary: 50000 + (i * 1000),
        status: 'Active',
        group: `Group ${i % 10}`,
        description: this.formatDate(descriptionDate)
      });
    }
  }

  getEmployees() {
    return this.employees;
  }

  addEmployee(employee: any) {
    employee.id = this.employees.length + 1;
    console.log(employee);
    
    this.employees.push(employee);
  }

  deleteEmployee(id: number): void {
    this.employees = this.employees.filter(employee => employee.id !== id);
  }
  
  randomDate(start: Date, end: Date): Date {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }

  formatDate(date: Date): string {
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();
    return `${month.toString().padStart(2, '0')}/${day.toString().padStart(2, '0')}/${year}`;
  }

  getEmployeeById(id: number): any {
    return this.employees.find(employee => employee.id === id);
  }
}
