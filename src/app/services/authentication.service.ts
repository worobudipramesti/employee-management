import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean = false;
  isLoggedInChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private router: Router
  ) { }

  login() {
    this.isLoggedIn = true;
    this.isLoggedInChanged.emit(true); 
  }

  logout() {
    this.isLoggedIn = false;
    this.router.navigate(['/login'])
    this.isLoggedInChanged.emit(false);
  }
}
