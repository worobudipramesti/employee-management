import { Component } from '@angular/core';
import { AuthService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'employee-management';
  isLoggedIn: boolean = false;

  constructor(
    private authService: AuthService
  ){  
    this.authService.isLoggedInChanged.subscribe((value: boolean) => {
      console.log('login: ', value);
      
      this.isLoggedIn = value;
    });  }

  logout(){
    this.authService.logout();
  }
}
