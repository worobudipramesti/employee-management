import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rupiah'
})
export class RupiahPipe implements PipeTransform {
  transform(value: number): string {
    if (value === null || value === undefined || isNaN(value)) return 'Rp. 0,00';

    if (typeof value !== 'number') {
      throw new Error('Value is not a number');
    }

    const formattedValue = value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.');
    const parts = formattedValue.split('.');

    if (parts.length > 1) {
      parts[parts.length - 1] = parts[parts.length - 1].replace('.', ',');
    }

    return `Rp. ${parts.join('.')}`;
  }
}
