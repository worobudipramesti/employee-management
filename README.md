# EmployeeManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.8.

# How to run it locally
1. clone the repository to your local machine:
```bash
$ git clone https://gitlab.com/worobudipramesti/employee-management.git
```

2. Run `npm install` inside the downloaded/cloned folder:
```bash
$ npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Feature
1. Login Page
- Gunakan input username dan password untuk login.
- Anda dapat menggunakan data hard-coded untuk login: username: admin, password: password.

2. Employee List Page
- Halaman daftar karyawan akan menampilkan setidaknya 100 data karyawan dengan paging, sorting, dan searching.
- Anda dapat mengatur jumlah data yang ditampilkan dalam 1 halaman.
- Terdapat tombol untuk menambahkan karyawan yang akan membawa Anda ke halaman "Add Employee".
- Di setiap baris data, terdapat tombol edit dan delete untuk mengedit atau menghapus data karyawan.
- Notifikasi aksi yang dilakukan (edit/delete) akan muncul dengan warna notifikasi yang berbeda.

3. Add Employee Page
- Halaman "Add Employee" memungkinkan Anda untuk menambahkan data karyawan baru.
- Seluruh atribut data karyawan bersifat mandatory dan validasi akan dilakukan untuk memastikan tidak ada field yang kosong.
- Input birthDate menggunakan datetime picker dan tidak boleh lebih besar dari hari ini.
- Input email akan divalidasi sesuai dengan format email yang benar.
- Input basicSalary harus berupa angka.
- Input group merupakan drop down list dengan opsi pencarian. Pilihan grup diisi dengan 10 nama grup dummy.

4. Employee Detail Page
- Halaman "Employee Detail" menampilkan detail data seorang karyawan dengan format data yang sudah diformat.
- Terdapat tombol 'ok' untuk kembali ke halaman Employee List, dan pencarian data sebelumnya akan tetap ada.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
